<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
    @csrf
      <p>First name:</p>
      <input type="text" name="firstName"/>
      <p>Last name:</p>
      <input type="text" name="lastName"/>
      <p>Gender:</p>
      <input type="radio" id="male" name="gender"/><label for="male">Male</label><br />
      <input type="radio" id="female" name="gender"/><label for="female">Female</label><br />
      <input type="radio" id="other" name="gender"/><label for="other">Other</label><br />
      <p>Nationality:</p>
      <select name="" id="">
        <option value="">Indonesian</option>
        <option value="">Singaporean</option>
        <option value="">Malaysian</option>
        <option value="">American</option>
      </select>
      <p>Bio:</p>
      <textarea name="" id="" cols="30" rows="10"></textarea>
      <br />
      <button type="submit">Sign Up</button> 
    </form>
</body>
</html>